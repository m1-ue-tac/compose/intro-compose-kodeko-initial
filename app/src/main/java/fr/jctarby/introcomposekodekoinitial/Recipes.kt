package fr.jctarby.introcomposekodekoinitial

import fr.jctarby.introcomposekodekoinitial.Recipe

val defaultRecipes = listOf(
    Recipe(imageResource = R.drawable.noodles, title = "Ramen", ingredients = listOf("Noodles", "Eggs", "Mushrooms", "Carrots", "Soy Sauce")),
    Recipe(R.drawable.croissant, "Croissant", listOf("Butter", "More Butter", "A touch of Butter", "Flour")),
    Recipe(R.drawable.pizza, "Pizza", listOf("Pizza Dough", "Tomatoes", "Cheese", "Spinach", "Love")),
    Recipe(R.drawable.produce, "Vegetable Medley", listOf("Vegetables")),
    Recipe(R.drawable.salad_egg, "Egg Salad", listOf("Eggs", "Mayonnaise", "Paprika", "Mustard")),
    Recipe(R.drawable.smoothie, "Fruit Smoothie", listOf("Banana", "Kiwi", "Milk", "Cream", "Ice", "Flax seed"))
)
