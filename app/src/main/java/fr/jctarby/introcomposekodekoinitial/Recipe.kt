package fr.jctarby.introcomposekodekoinitial

data class Recipe(
//    @DrawableRes val imageResource: Int,
    val imageResource: Int,
    val title: String,
    val ingredients: List<String>
)
